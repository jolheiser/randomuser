package field

// Field is a randomuser.User field
type Field string

var (
	Gender     Field = "gender"
	Name       Field = "name"
	Location   Field = "location"
	Email      Field = "email"
	Login      Field = "login"
	Registered Field = "registered"
	DOB        Field = "dob"
	Phone      Field = "phone"
	Cell       Field = "cell"
	ID         Field = "id"
	Picture    Field = "picture"
	NAT        Field = "nat"
)
