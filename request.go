package randomuser

import (
	"fmt"
	"net/url"
	"strings"

	"go.jolheiser.com/randomuser/field"
	"go.jolheiser.com/randomuser/nat"
	"go.jolheiser.com/randomuser/password"
)

// RequestOptions is the request options
// https://randomuser.me/documentation
type RequestOptions struct {
	Results  int
	Page     int
	Gender   string
	Password password.Options
	Seed     string
	NAT      []nat.NAT
	Include  []field.Field
	Exclude  []field.Field
}

// Values converts RequestOptions into url.Values
func (r RequestOptions) Values() url.Values {
	values := make(url.Values)

	if r.Results > 0 {
		values["results"] = str(r.Results)
	}
	if r.Page > 0 {
		values["page"] = str(r.Page)
	}
	if r.Gender != "" {
		values["gender"] = str(r.Gender)
	}
	if !r.Password.IsValid() {
		values["password"] = str(r.Password)
	}
	if r.Seed != "" {
		values["seed"] = str(r.Seed)
	}
	if r.NAT != nil && len(r.NAT) > 0 {
		nats := make([]string, len(r.NAT))
		for idx, n := range r.NAT {
			nats[idx] = string(n)
		}
		values["nat"] = str(strings.Join(nats, ","))
	}
	if r.Include != nil && len(r.Include) > 0 {
		includes := make([]string, len(r.Include))
		for idx, i := range r.Include {
			includes[idx] = string(i)
		}
		values["inc"] = str(strings.Join(includes, ","))
	}
	if r.Exclude != nil && len(r.Exclude) > 0 {
		excludes := make([]string, len(r.Exclude))
		for idx, e := range r.Exclude {
			excludes[idx] = string(e)
		}
		values["exc"] = str(strings.Join(excludes, ","))
	}

	return values
}

func str(i interface{}) []string {
	return []string{fmt.Sprintf("%v", i)}
}
