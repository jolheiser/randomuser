package randomuser

// Response is the response from https://randomuser.me
type Response struct {
	Results []User `json:"results"`
	Info    Info   `json:"info"`
	Error   string `json:"error"`
}

// Info is the info portion of the Response
type Info struct {
	Seed    string `json:"seed"`
	Results int    `json:"results"`
	Page    int    `json:"page"`
	Version string `json:"version"`
}
