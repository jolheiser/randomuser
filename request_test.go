package randomuser

import (
	"net/url"
	"os"
	"testing"

	"go.jolheiser.com/randomuser/field"
	"go.jolheiser.com/randomuser/nat"
	"go.jolheiser.com/randomuser/password"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestRequest(t *testing.T) {
	tt := []struct {
		Name    string
		Request RequestOptions
		Values  url.Values
	}{
		{
			Name: "results",
			Request: RequestOptions{
				Results: 10,
			},
			Values: url.Values{
				"results": []string{"10"},
			},
		},
		{
			Name: "page",
			Request: RequestOptions{
				Page: 5,
			},
			Values: url.Values{
				"page": []string{"5"},
			},
		},
		{
			Name: "gender",
			Request: RequestOptions{
				Gender: "male",
			},
			Values: url.Values{
				"gender": []string{"male"},
			},
		},
		{
			Name: "password",
			Request: RequestOptions{
				Password: password.Options{
					Charsets: []password.Charset{
						password.Lower,
						password.Upper,
					},
					MinLength: 5,
					MaxLength: 15,
				},
			},
			Values: url.Values{
				"password": []string{"lower,upper,5-15"},
			},
		},
		{
			Name: "seed",
			Request: RequestOptions{
				Seed: "s33d",
			},
			Values: url.Values{
				"seed": []string{"s33d"},
			},
		},
		{
			Name: "nat",
			Request: RequestOptions{
				NAT: []nat.NAT{
					nat.AU,
					nat.US,
				},
			},
			Values: url.Values{
				"nat": []string{"au,us"},
			},
		},
		{
			Name: "include",
			Request: RequestOptions{
				Include: []field.Field{
					field.Name,
					field.Location,
				},
			},
			Values: url.Values{
				"inc": []string{"name,location"},
			},
		},
		{
			Name: "exclude",
			Request: RequestOptions{
				Exclude: []field.Field{
					field.NAT,
					field.ID,
				},
			},
			Values: url.Values{
				"exc": []string{"nat,id"},
			},
		},
		{
			Name: "all",
			Request: RequestOptions{
				Results: 1500,
				Page:    2,
				Gender:  "female",
				Password: password.Options{
					Charsets: []password.Charset{
						password.Upper,
						password.Special,
						password.Number,
					},
					MinLength: 5,
					MaxLength: 25,
				},
				Seed: "1337s33d",
				NAT: []nat.NAT{
					nat.US,
					nat.BR,
					nat.CA,
				},
				Include: []field.Field{
					field.Name,
					field.Phone,
					field.Picture,
				},
				Exclude: []field.Field{
					field.NAT,
					field.Location,
					field.Registered,
				},
			},
			Values: url.Values{
				"results":  []string{"1500"},
				"page":     []string{"2"},
				"gender":   []string{"female"},
				"password": []string{"upper,special,number,5-25"},
				"seed":     []string{"1337s33d"},
				"nat":      []string{"us,br,ca"},
				"inc":      []string{"name,phone,picture"},
				"exc":      []string{"nat,location,registered"},
			},
		},
	}

	for _, tc := range tt {
		t.Run(tc.Name, func(t *testing.T) {
			if !equal(tc.Request.Values(), tc.Values) {
				t.Logf("\nExpected: %v\nGot: %v\n", tc.Values, tc.Request.Values())
				t.Fail()
			}
		})
	}
}

func equal(u1, u2 url.Values) bool {
	for k1, v1 := range u1 {
		v2, ok := u2[k1]
		if !ok {
			return false
		}
		if len(v1) != len(v2) {
			return false
		}
		for _, vv1 := range v1 {
			var match bool
			for _, vv2 := range v2 {
				if vv1 == vv2 {
					match = true
					break
				}
			}
			if !match {
				return false
			}
		}
	}
	return true
}
