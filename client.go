package randomuser

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)

const endpoint = "https://randomuser.me/api/1.3/"

// Client is a gateway to https://randomuser.me
type Client struct {
	http *http.Client
}

// ClientOption is any func that can operate on a Client
type ClientOption func(*Client)

// New returns a new Client
func New(opts ...ClientOption) *Client {
	c := &Client{
		http: http.DefaultClient,
	}

	for _, opt := range opts {
		opt(c)
	}

	return c
}

// WithHTTP sets the http.Client to use with a Client
func WithHTTP(client *http.Client) ClientOption {
	return func(c *Client) {
		c.http = client
	}
}

// Query returns a Response
func (c *Client) Query(opt RequestOptions) (*Response, error) {
	resp, err := c.http.Get(fmt.Sprintf("%s?%s", endpoint, opt.Values().Encode()))
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("non-200 response: %s", resp.Status)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var res Response
	if err := json.Unmarshal(body, &res); err != nil {
		return nil, err
	}

	if res.Error != "" {
		return &res, errors.New(res.Error)
	}

	return &res, nil
}
