package password

import (
	"fmt"
	"strconv"
	"strings"
)

// Options is for password options
type Options struct {
	Charsets  []Charset
	MinLength int
	MaxLength int
}

// IsValid checks if Options is valid
func (o Options) IsValid() bool {
	return (o.Charsets == nil || len(o.Charsets) == 0) && o.MinLength == 0 && o.MaxLength == 0
}

// String implements fmt.Stringer
func (o Options) String() string {
	if len(o.Charsets) == 0 {
		return ""
	}

	charsets := make([]string, len(o.Charsets))
	for idx, ch := range o.Charsets {
		charsets[idx] = string(ch)
	}
	minMax := ""
	if o.MaxLength > 0 {
		minMax = strconv.Itoa(o.MaxLength)
		if o.MinLength > 0 {
			minMax = fmt.Sprintf("%d-%s", o.MinLength, minMax)
		}
	}

	res := strings.Join(charsets, ",")
	if minMax != "" {
		res += fmt.Sprintf(",%s", minMax)
	}
	return res
}
