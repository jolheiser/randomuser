package password

import (
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestOptions(t *testing.T) {
	tt := []struct {
		Name  string
		Opts  Options
		Value string
	}{
		{
			Name: "charsets",
			Opts: Options{
				Charsets: []Charset{
					Lower,
					Upper,
				},
			},
			Value: "lower,upper",
		},
		{
			Name: "min",
			Opts: Options{
				MinLength: 10,
			},
		},
		{
			Name: "max-invalid",
			Opts: Options{
				MaxLength: 50,
			},
		},
		{
			Name: "max-valid",
			Opts: Options{
				Charsets: []Charset{
					Lower,
				},
				MaxLength: 50,
			},
			Value: "lower,50",
		},
		{
			Name: "all",
			Opts: Options{
				Charsets: []Charset{
					Special,
					Number,
				},
				MinLength: 10,
				MaxLength: 50,
			},
			Value: "special,number,10-50",
		},
	}

	for _, tc := range tt {
		t.Run(tc.Name, func(t *testing.T) {
			value := tc.Opts.String()
			if value != tc.Value {
				t.Logf("\nExpected: %s\nGot: %s\n", tc.Value, value)
				t.Fail()
			}
		})
	}
}
