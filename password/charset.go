package password

// Charset is a password charset
type Charset string

var (
	Special Charset = "special"
	Upper   Charset = "upper"
	Lower   Charset = "lower"
	Number  Charset = "number"
)
