package nat

// NAT is a randomuser.User nat
type NAT string

var (
	AU NAT = "au"
	BR NAT = "br"
	CA NAT = "ca"
	CH NAT = "ch"
	DE NAT = "de"
	DK NAT = "dk"
	ES NAT = "es"
	FI NAT = "fi"
	FR NAT = "fr"
	GB NAT = "gb"
	IE NAT = "ie"
	IR NAT = "ir"
	NO NAT = "no"
	NL NAT = "nl"
	NZ NAT = "nz"
	TR NAT = "tr"
	US NAT = "us"
)
