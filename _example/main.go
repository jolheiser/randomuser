package main

import (
	"fmt"
	"html/template"
	"net/http"

	"go.jolheiser.com/randomuser"
)

func main() {
	http.HandleFunc("/", handler)

	fmt.Println("http://localhost:1337")
	panic(http.ListenAndServe(":1337", nil))
}

func handler(w http.ResponseWriter, _ *http.Request) {
	resp, err := client.Query(randomuser.RequestOptions{})
	if err != nil {
		w.WriteHeader(500)
		return
	}

	_ = tmpl.Execute(w, resp.Results[0])
}

var (
	client = randomuser.New()
	tmpl = template.Must(template.New("example").Parse(html))
	html = `
<html>
<head>
<title>{{.FullName false}}</title>
</head>

<body>
<h1>{{.FullName true}}</h1>
<img src="{{.Picture.Large}}"/>

<h2>Contact</h2>
<p>Username: @{{.Login.Username}}</p>
<p>Phone: {{.Phone}}</p>
<p>Email: {{.Email}}</p>
<p>Address:<br/>
{{.Location.Street.Number}} {{.Location.Street.Name}}<br/>
{{.Location.City}}, {{.Location.State}}<br/>
{{.Location.Country}} {{.Location.PostCode}}
</p>

</body>
</html>
`
)