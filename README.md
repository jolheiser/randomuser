# randomuser

A library for interacting with [randomuser](https://randomuser.me).

**NOTE:** This library currently uses the `v1.3` API.

## Example

See the [example](_example).

## License

[MIT](LICENSE)